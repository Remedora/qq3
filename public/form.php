<!DOCTYPE html>

<html lang="en">
<head>
    <title>laba 3</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>Форма</header>
    <form method="post" action="index.php">
        <label> Введите Имя: <input type="text" name="name" placeholder="Введите имя"></label>

        <label> Введите Email: <input type="email" name="email" placeholder="Введите e-mail"></label>

        <label> Введите дату рождения: <input type="date" name="date"></label>

        <label>
            Выберите пол: 
            <input type="radio" name="gender" value="Мужской"> Мужской
            <input type="radio" name="gender" value="Женский"> Женский
        </label>

        <label>
            Выберите кол-во конечностей: 
            <input type="radio" name="limb" value="Значение 1"> 1
            <input type="radio" name="limb" value="Значение 2"> 2
            <input type="radio" name="limb" value="Значение 3"> 3
            <input type="radio" name="limb" value="Значение 4"> 4
        </label>

        <label>
            Выберите сверхспособности: 
            <select name="superspos" multiple="multiple">
                <option value="Значение 1"> Бессмертие </option>
                <option value="Значение 2"> Прохождение сквозь стены </option>
                <option value="Значение 3"> Левитация </option>
            </select>
        </label>

        <label>
            Биография: 
            <textarea name="biography" placeholder="Введите информацию о себе"></textarea>
        </label>

        <label> С контрактом ознакомлен <input type="checkbox" name="agree"> </label>
  
        <label><button type="submit"> Отправить </button></label>
    </form>
</body>
</html> 
