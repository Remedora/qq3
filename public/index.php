<?php
/**
 * Интернет-программирование. Задача 8.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}

$name = $_POST["name"];
$email = $_POST["email"];
$date = $_POST["date"];
$gender = $_POST["gender"];
$limb = $_POST["limb"];
$biography = $_POST["biography"]; 
$agree = $_POST["agree"];
$flag=FALSE;
$errors = FALSE;
$errors = FALSE;

if (empty($name)) {
  echo  "ОШИБКА!: Введите ваше имя:<br/>";
  $errors = TRUE;
}else if(!preg_match("#^[aA-zZ0-9\-_]+$#",$_POST["name"])){
  print('ОШИБКА!: Символы введены некорректно!<br/>');
  $errors=TRUE;
}
if (empty($email)){
  echo "ОШИБКА!: Вы не ввели email!<br/>";
  $errors = TRUE;
}
if  (empty($date)){
  echo "ОШИБКА!: Вы не ввели дату рождения!<br/>";
  $errors = TRUE;
}
if  (empty($_POST["gender"])){
  echo "ОШИБКА!: Вы не выбрали пол!<br/>";
  $errors = TRUE;
}

$superspos1 = $_POST["superspos"];
  if(!isset($superspos1))
  {
    echo("<p>ОШИБКА!: Вы не выбрали сверхспособности!</p>\n");
      $errors = TRUE;
  }
$superspos_separated=$superspos1;
if  (empty($_POST["biography"])){
  echo "ОШИБКА!: Заполните поле биографии!<br/>";
  $errors = TRUE;
}
if  (empty($_POST["agree"])){
  echo "ОШИБКА!: Нет соглашения с условиями!<br/>";
  $errors = TRUE; 
}

if($errors){
  exit();
}
$user = 'u20300';
$pass = '5434493';
$db = new PDO('mysql:host=localhost;dbname=u20300', $user, $pass,
array(PDO::ATTR_PERSISTENT => true));
try {
 $stmt = $db->prepare("INSERT INTO qq (name, email, date, gender, limb, superspos, biography, agree) 
 VALUES (:name, :email, :date, :gender, :limb, :superspos, :biography, :agree)");
$stmt->bindParam(':name', $name_db);
$stmt->bindParam(':email', $email_db);
$stmt->bindParam(':date', $date_db);
$stmt->bindParam(':gender', $gender_db);
$stmt->bindParam(':limb', $limb_db);
$stmt->bindParam(':superspos', $superspos_db);
$stmt->bindParam(':biography', $biography_db);
$stmt->bindParam(':agree', $agree_db);
$name_db=$_POST["name"];
$email_db=$_POST["email"];
$date_db=$_POST["date"];
$gender_db=$_POST["gender"];
$limb_db=$_POST["limb"];
$superspos_db=$superspos_separated;
$biography_db=$_POST["biography"];
$agree_db=$_POST["agree"];
$stmt->execute();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
header('Location: ?save=1');
?>